﻿namespace MWC.Livelink.Interface
{
    using System;

    class AdapterException : Exception
    {
        public AdapterException(string message) : base(message)
        {
        }

        public AdapterException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
