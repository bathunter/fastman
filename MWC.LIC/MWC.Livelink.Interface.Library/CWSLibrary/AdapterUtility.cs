﻿namespace MWC.Livelink.Interface.CWSLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Support_CWS_16_22;
    using static CWSClient;
    using static DocManClient;

    class AdapterUtility
    {
        public static Category FromCat(Support_CWS_16_22.Node cwsCat)
        {
            Category livelinkCategory = new Category();
            livelinkCategory.ID = (int)cwsCat.ID;
            livelinkCategory.Name = cwsCat.Name;
            livelinkCategory.VolumeID = (int)cwsCat.VolumeID;
            livelinkCategory.ParentID = (int)cwsCat.ParentID;
            if (cwsCat.CreateDate.HasValue)
            {
                livelinkCategory.CreatedDate = cwsCat.CreateDate.Value;
            }

            if (cwsCat.ModifyDate.HasValue)
            {
                livelinkCategory.ModifiedDate = cwsCat.ModifyDate.Value;
            }

            return livelinkCategory;
        }

        public static Folder From(Support_CWS_16_22.Node cwsNode, Callback nodeCallBack)
        {
            Folder livelinkFolder = new Folder();
            livelinkFolder.ID = (int)cwsNode.ID;
            livelinkFolder.Name = cwsNode.Name;
            livelinkFolder.VolumeID = (int)cwsNode.VolumeID;
            livelinkFolder.ParentID = (int)cwsNode.ParentID;
            if (cwsNode.CreateDate.HasValue)
            {
                livelinkFolder.CreatedDate = cwsNode.CreateDate.Value;
            }

            if (cwsNode.ModifyDate.HasValue)
            {
                livelinkFolder.ModifiedDate = cwsNode.ModifyDate.Value;
            }

            livelinkFolder.Categories = From(cwsNode.Metadata, nodeCallBack);

            return livelinkFolder;
        }


        public static CategoriesCollection From(Metadata metadata, Callback nodeCallBack)
        {
            CategoriesCollection categories = new CategoriesCollection();

            if (metadata != null && metadata.AttributeGroups != null)
            {
                foreach (AttributeGroup cwsMetadataAttributeGroup in metadata.AttributeGroups)
                {
                    if (cwsMetadataAttributeGroup.Type.Equals("Category"))
                    {
                        Category livelinkCategory = new Category();
                        livelinkCategory.Name = cwsMetadataAttributeGroup.DisplayName;
                        livelinkCategory.ID = (int)GetCatIdFromKey(cwsMetadataAttributeGroup.Key);

                        Support_CWS_16_22.Node catNode = nodeCallBack(livelinkCategory.ID);
                        livelinkCategory.VolumeID = (int)catNode.VolumeID;
                        livelinkCategory.ParentID = (int)catNode.ParentID;
                        if (catNode.CreateDate.HasValue)
                        {
                            livelinkCategory.CreatedDate = catNode.CreateDate.Value;
                        }

                        if (catNode.ModifyDate.HasValue)
                        {
                            livelinkCategory.ModifiedDate = catNode.ModifyDate.Value;
                        }

                        livelinkCategory.Attributes = new AttributesCollection();

                        if (cwsMetadataAttributeGroup.Values != null)
                        {
                            foreach (DataValue cwsAttribute in cwsMetadataAttributeGroup.Values)
                            {
                                livelinkCategory.Attributes.Add(From(cwsAttribute));
                            }
                        }

                        categories.Add(livelinkCategory);
                    }
                }
            }

            return categories;
        }

        public static Metadata From(CategoriesCollection collection, List<AttributeGroup> templates)
        {
            List<AttributeGroup> attrGroups = new List<AttributeGroup>();
            Metadata metadata = new Metadata();
            bool attrFound;
            bool catFound;

            foreach (Category category in collection)
            {
                catFound = false;
                category.ID = (category.ID > 0) ? category.ID : (int)templates.Where(s => s.DisplayName.Equals(category.Name.Trim())).Select(s => GetCatIdFromKey(s.Key)).FirstOrDefault();
                foreach (AttributeGroup template in templates)
                {
                    if (category.ID == GetCatIdFromKey(template.Key))
                    {
                        foreach (Interface.Attribute attr in category.Attributes)
                        {
                            attrFound = false;
                            attr.ID = (attr.ID > 0) ? attr.ID : template.Values.Where(s => s.Description.Equals(attr.Name.Trim())).Select(s => GetAttrIdFromKey(s.Key)).FirstOrDefault();
                            for (int i = 0; i < template.Values.Length; i++)
                            {
                                if (GetAttrIdFromKey(template.Values[i].Key) == attr.ID)
                                {
                                    try
                                    {
                                        SetMetadataAttribute(template.Values[i], attr.Value);
                                        attrFound = true;
                                        break;
                                    }
                                    catch (AdapterException e)
                                    {
                                        throw new AdapterException(string.Format("{0} -> {1}\n", category.Name, template.DisplayName), e);
                                    }
                                }
                            }

                            if (!attrFound)
                            {
                                throw new LivelinkAttributeDoesNotExistException(string.Format("Attribute(Name:'{0}' Value:'{1}' Category:'{2}')", attr.Name, attr.Value, template.DisplayName));
                            }
                        }

                        attrGroups.Add(template);
                        catFound = true;
                        break;
                    }
                }

                if (!catFound)
                {
                    throw new AdapterException(string.Format("Category {0} isn't found in templates: ({1})", category.Name, templates.Select(s => s.DisplayName).ToString()));
                }
            }

            metadata.AttributeGroups = attrGroups.ToArray();

            return metadata;
        }

        private static Interface.Attribute From(DataValue cwsAttribute)
        {
            Interface.Attribute attribute = new Interface.Attribute();
            attribute.Name = cwsAttribute.Description;
            attribute.ID = GetAttrIdFromKey(cwsAttribute.Key);
            attribute.Value = SetLivelinkAttributeValue(cwsAttribute);
            return attribute;
        }

        private static object SetLivelinkAttributeValue(DataValue cwsAttribute)
        {
            object valueToSet = null;
            if (cwsAttribute is StringValue)
            {
                valueToSet = ((StringValue)cwsAttribute).Values?[0];
            }
            else if (cwsAttribute is IntegerValue)
            {
                valueToSet = ((IntegerValue)cwsAttribute).Values?[0];
            }
            else if (cwsAttribute is BooleanValue)
            {
                valueToSet = ((BooleanValue)cwsAttribute).Values?[0];
            }
            else if (cwsAttribute is DataValue)
            {
                valueToSet = ((DateValue)cwsAttribute).Values?[0];
            }

            return valueToSet;
        }

        private static void SetMetadataAttribute(DataValue dataValue, object value)
        {
            try
            {
                if (dataValue is StringValue)
                {
                    StringValue stringValue = dataValue as StringValue;
                    if (value.GetType().IsArray)
                    {
                        stringValue.Values = (string[])value;
                    }
                    else
                    {
                        stringValue.Values = new string[] { value.ToString() };
                    }
                }
                else if (dataValue is IntegerValue)
                {
                    IntegerValue intValue = dataValue as IntegerValue;
                    if (value.GetType().IsArray)
                    {
                        intValue.Values = (long?[])value;
                    }
                    else
                    {
                        intValue.Values = new long?[] { (long?)((value != null) ? Convert.ToInt64(value) : value) };
                    }
                }
                else if (dataValue is BooleanValue)
                {
                    BooleanValue boolValue = dataValue as BooleanValue;
                    if (value.GetType().IsArray)
                    {
                        boolValue.Values = (bool?[])value;
                    }
                    else
                    {
                        boolValue.Values = new bool?[] { value as bool? };
                    }
                }
                else if (dataValue is DateValue)
                {
                    DateValue dateValue = dataValue as DateValue;
                    if (value.GetType().IsArray)
                    {
                        dateValue.Values = (DateTime?[])value;
                    }
                    else
                    {
                        dateValue.Values = new DateTime?[] { value as DateTime? };
                    }
                }
                else
                {
                    throw new AdapterException(string.Format("Attribute {0} couldn't be cast to any of existing type", dataValue.Description));
                }
            }
            catch (Exception e)
            {
                throw new AdapterException(string.Format("{0} : {1}\n", dataValue.Description, e.Message), e);
            }
        }
    }
}
