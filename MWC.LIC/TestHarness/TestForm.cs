﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Web.Services.Protocols;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Net;

namespace MWC.Livelink.Interface.WebService.TestHarness
{
    public partial class TestForm : Form
    {
        const string GetFolder = "GetFolder";
        const string CreateFolder = "CreateFolder";
        const string CreateFolderStructure = "CreateFolderStructure";
        const string GetInstanceSettings = "GetInstanceSettings";
        const string GetInstanceCategories = "GetInstanceCategories";
        const string EncryptString = "EncryptString";

        MWCLIC.MWCLICWebService ws = null;

        UserControl currentControl = null;

        public TestForm()
        {
            InitializeComponent();
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            System.Reflection.AssemblyName n = a.GetName();
            this.Text = string.Format("{0} v.{1}.{2}.{3}", this.Text, n.Version.Major, n.Version.Minor, n.Version.Build);
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            LoadControls();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            ClearTempFiles();
            base.OnFormClosing(e);
        }

        private void LoadControls()
        {
            ws = new MWCLIC.MWCLICWebService();
            textBoxWebServiceURL.Text = ws.Url;
            ws.Credentials = CredentialCache.DefaultCredentials;
            comboBoxWebMethods.Items.Add(GetInstanceSettings);
            comboBoxWebMethods.Items.Add(GetInstanceCategories);
            comboBoxWebMethods.Items.Add(GetFolder);
            comboBoxWebMethods.Items.Add(CreateFolder);
            comboBoxWebMethods.Items.Add(CreateFolderStructure);
            comboBoxWebMethods.Items.Add(EncryptString);
            
        }

        private void comboBoxWebMethods_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxWebMethods.SelectedItem == null) return;
            panelFunction.Controls.Clear();
            switch (comboBoxWebMethods.SelectedItem.ToString())
            {
                case GetFolder:
                    currentControl = new GetFolder();
                    break;
                case CreateFolder:
                    currentControl = new CreateFolder();
                    break;
                case CreateFolderStructure:
                    currentControl = new CreateFolderStructure();
                    break;
                case GetInstanceCategories:
                    currentControl = new GetInstanceCategories();
                    break;
                case GetInstanceSettings:
                    currentControl = new BaseControl();
                    ((BaseControl)currentControl).livelinkSession1.Visible = false;
                    break;
                case EncryptString:
                    currentControl = new Encrypt();
                    break;
                default:
                    currentControl = null;
                    break;
            }
            if(currentControl!=null )
            {
                if (currentControl is BaseControl)
                {
                    ((BaseControl)currentControl).RunClicked += new BaseControl.RunClickedHandler(control_RunClicked);
                    ((BaseControl)currentControl).CancelClicked += new BaseControl.CancelClickedHandler(control_CancelClicked);
                }
                currentControl.Dock = DockStyle.Fill;
                panelFunction.Controls.Add(currentControl);

            }
        }

        private void control_CancelClicked()
        {
            panelFunction.Controls.Clear();
            comboBoxWebMethods.SelectedIndex = -1;
            webBrowser1.Navigate("about:blank");
        }

        private void control_RunClicked()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                switch (comboBoxWebMethods.SelectedItem.ToString())
                {
                    case GetFolder:
                        RunGetFolder();
                        break;
                    case CreateFolder:
                        RunCreateFolder();
                        break;
                    case CreateFolderStructure:
                        RunCreateFolderStructure();
                        break;
                    case GetInstanceCategories:
                        RunGetInstanceCategories();
                        break;
                    case GetInstanceSettings:
                        RunGetInstanceSettings();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //unhandled exception
                labelRun.ForeColor = Color.Red;
                labelRun.Text = string.Format("ERROR: {0}",ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void RunGetInstanceSettings()
        {
            try
            {
                MWCLIC.InstanceSettings settings = ws.GetInstanceSettings();
                DisplayResult(typeof(MWCLIC.InstanceSettings), settings);
            }
            catch (SoapException ex)
            {
                DisplaySoapError(ex);
            }

        }


        private void RunGetInstanceCategories()
        {
            try
            {
                MWCLIC.Category[] categories = ws.GetInstanceCategories(GetSession());
                DisplayResult(typeof(MWCLIC.Category[]), categories);
            }
            catch (SoapException ex)
            {
                DisplaySoapError(ex);
            }

        }

        private void RunGetFolder()
        {
            try
            {
                MWCLIC.Folder folder = ws.GetFolder(GetSession(), Convert.ToInt32(((GetFolder)currentControl).textBoxFolderID.Text));
                DisplayResult(typeof(MWCLIC.Folder), folder);

            }
            catch (SoapException ex)
            {
                DisplaySoapError(ex);
            }

        }

        private void RunCreateFolder()
        {
           try
            {
                MWCLIC.Folder folder = ws.CreateFolder(GetSession(), Convert.ToInt32(((CreateFolder)currentControl).textBoxParentID.Text), (MWCLIC.Folder)((CreateFolder)currentControl).folderInfo.Folders.GetValue(0), ((MWCLIC.NodeExistsCreateMode)((CreateFolder)currentControl).comboBoxNodeExistCreateMode.SelectedItem));
                DisplayResult(typeof(MWCLIC.Folder), folder);

            }
            catch (SoapException ex)
            {
                DisplaySoapError(ex);
            }
        }

        private void RunCreateFolderStructure()
        {
            try
            {
                MWCLIC.Folder[] folders = ws.CreateFolderStructure(GetSession(), Convert.ToInt32(((CreateFolderStructure)currentControl).textBoxParentID.Text), ((CreateFolderStructure)currentControl).folderInfo.Folders, ((MWCLIC.NodeExistsCreateMode)((CreateFolderStructure)currentControl).comboBoxNodeExistCreateMode.SelectedItem));
                DisplayResult(typeof(MWCLIC.Folder[]), folders);

            }
            catch (SoapException ex)
            {
                DisplaySoapError(ex);
            }
        }


        private MWCLIC.LivelinkSession GetSession()
        {
            MWCLIC.LivelinkSession livelinkSession = new MWCLIC.LivelinkSession();
            if (((BaseControl)currentControl).livelinkSession1.radioButtonSSO.Checked)
            {
                livelinkSession.Authentication = MWCLIC.AuthenticationType.SingleSignOn;
            }
            else
            {
                livelinkSession.Authentication = MWCLIC.AuthenticationType.Livelink;
                livelinkSession.LivelinkUser = ((BaseControl)currentControl).livelinkSession1.textBoxLivelinkUser.Text;
                livelinkSession.Password = ((BaseControl)currentControl).livelinkSession1.textBoxPassword.Text;
                livelinkSession.ImpersonateUser = ((BaseControl)currentControl).livelinkSession1.textBoxImpersonateUser.Text;
            }
            return livelinkSession;
        }

        private void DisplayResult(Type resultType, object result)
        {
            //dispaly result
            XmlSerializer serializer = new XmlSerializer(resultType);
            string xmlFile = Path.Combine(Path.GetTempPath(), string.Format("mwclic{0}.xml", Guid.NewGuid()));
            TextWriter tw = new StreamWriter(xmlFile);
            serializer.Serialize(tw, result);
            tw.Close();

            webBrowser1.Navigate(xmlFile);

            labelRun.ForeColor = Color.Green;
            labelRun.Text = "SUCCESS";
        }
       
        private void DisplaySoapError(SoapException ex)
        {
            //display error
            XmlSerializer serializer = new XmlSerializer(typeof(XmlNode));
            string xmlFile = Path.Combine(Path.GetTempPath(), string.Format("mwclic{0}.xml", Guid.NewGuid()));
            TextWriter tw = new StreamWriter(xmlFile);
            serializer.Serialize(tw, ex.Detail);
            tw.Close();

            webBrowser1.Navigate(xmlFile);

            //Load the Detail element of the SoaopException object   
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ex.Detail.OuterXml);
            XmlNamespaceManager nsManager = new XmlNamespaceManager(doc.NameTable);
            // Add the namespace to the NamespaceManager  
            nsManager.AddNamespace("errorNS", "http://www.melbournewater.com.au/webservices/");
            XmlNode errorNode = doc.DocumentElement.SelectSingleNode("errorNS:Error", nsManager);

            labelRun.ForeColor = Color.Red;
            labelRun.Text = string.Format("ERROR: {0}", errorNode.SelectSingleNode("errorNS:ErrorType", nsManager).InnerText);

        }

        private void ClearTempFiles()
        {
            try
            {
                string[] tempFiles = Directory.GetFiles(Path.GetTempPath(), "mwclic*.xml");
                foreach (string file in tempFiles)
                {
                    File.Delete(file);
                }
            }
            catch
            { }

        }

        private void textBoxWebServiceURL_Validated(object sender, EventArgs e)
        {
            //TODO: check url is valid
            ws.Url = textBoxWebServiceURL.Text;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
