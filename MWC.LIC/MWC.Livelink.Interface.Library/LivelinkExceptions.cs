﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MWC.Livelink.Interface
{
    #region Exceptions
    /// <summary>
    /// Base Livelink exception.
    /// </summary>
    public abstract class LivelinkBaseException : System.Exception
    {
        public LivelinkBaseException(string message) : base(message) { }
        protected int _exceptionCode;
        public int ExceptionCode
        {
            get
            {
                return _exceptionCode;
            }
        }
    }
    /// <summary>
    /// Unhandled LAPI exception.
    /// </summary>
    public class UnhandledLivelinkException : LivelinkBaseException
    {
        public UnhandledLivelinkException(string message) : base(message) { _exceptionCode = 1; }
    }
    /// <summary>
    /// Could not connect to livelink server.
    /// Check Livelink connection information supplied in LivelinkInstance object.
    /// Check if Livelink instace is up and running by accessing it via a browser.
    /// </summary>
    public class LivelinkConnectionException : LivelinkBaseException
    {
        public LivelinkConnectionException(string message) : base(message) { _exceptionCode = 2; }
    }
    /// <summary>
    /// Node already exists in Livelink.
    /// The Node you are trying to create already exists in the container you are trying to create it in.
    /// </summary>
    public class LivelinkNodeExistsException : LivelinkBaseException
    {
        private int _nodeid;
        private int _volumeid;

        public LivelinkNodeExistsException(string message) : base(message) { _exceptionCode = 3; }
        public LivelinkNodeExistsException(string message, int nodeID, int volumeID)
            : base(message)
        {
            _nodeid = nodeID;
            _volumeid = volumeID;
        }

        /// <summary>
        /// Node ID of the existing node
        /// </summary>
        public int NodeID
        {
            get
            {
                return _nodeid;
            }
        }
        /// <summary>
        /// Volume ID of the existing node
        /// </summary>
        public int VolumeID
        {
            get
            {
                return _volumeid;
            }
        }
    }
    /// <summary>
    /// Node does not exists in Livelink.
    /// The Node you are trying to retreive does not exists. Check node ID and Name and check if it exists in the
    /// container you are looking in.
    /// </summary>
    public class LivelinkNodeDoesNotExistException : LivelinkBaseException
    {
        public LivelinkNodeDoesNotExistException(string message) : base(message) { _exceptionCode = 4; }
    }
    /// <summary>
    /// Mandatory Attribute value is not supplied and cannot be found on parent folder.
    /// </summary>
    public class LivelinkMandatoryAttributeMissingException : LivelinkBaseException
    {
        public LivelinkMandatoryAttributeMissingException(string message) : base(message) { _exceptionCode = 5; }
    }
    /// <summary>
    /// Invalid node object.
    /// </summary>
    public class LivelinkInvalidNodeException : LivelinkBaseException
    {
        public LivelinkInvalidNodeException(string message) : base(message) { _exceptionCode = 6; }
    }
    /// <summary>
    /// Category does not exists in Livelink.
    /// The Category you are trying to retreive does not exists. Check category ID and Name and check if it exists in the
    /// Categories Volume you are looking in.
    /// </summary>
    public class LivelinkCategoryDoesNotExistException : LivelinkBaseException
    {
        public LivelinkCategoryDoesNotExistException(string message) : base(message) { _exceptionCode = 7; }
    }
    /// <summary>
    /// Category you are trying to update is not applied to the node.
    /// Check the node and apply the category before you try to update it.
    /// </summary>
    public class LivelinkNodeCategoryMissingException : LivelinkBaseException
    {
        public LivelinkNodeCategoryMissingException(string message) : base(message) { _exceptionCode = 8; }
    }
    /// <summary>
    /// Attribute does not exist in the category.
    /// The supplied attribute name is invalid.
    /// </summary>
    public class LivelinkAttributeDoesNotExistException : LivelinkBaseException
    {
        public LivelinkAttributeDoesNotExistException(string message) : base(message) { _exceptionCode = 9; }
    }
    /// <summary>
    /// Session could not be created
    /// Check all the connection information you pass to the session is correct
    /// Most often this error is due to invalid user name or password,
    /// or trying to connect to a single sign on enabled Livelink instance
    /// with user name that does not exist in Livelink.
    /// </summary>
    public class LivelinkSessionCreationException : LivelinkBaseException
    {
        public LivelinkSessionCreationException(string message) : base(message) { _exceptionCode = 10; }
    }
    /// <summary>
    /// Check the permissions of the user you are connecting with
    /// for the object you are trying to read
    /// </summary>
    public class LivelinkNotEnoughPermissionsException : LivelinkBaseException
    {
        public LivelinkNotEnoughPermissionsException(string message) : base(message) { _exceptionCode = 11; }
    }
    #endregion
}
