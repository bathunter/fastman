﻿namespace MWC.Livelink.Interface.CWSLibrary
{
    using Support_CWS_16_22;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using static CWSClient;

    class DocManClient : DocumentManagementClient
    {
        private readonly long enterpriseVolumeID;

        private readonly long categoryVolumeID;

        private OTAuthentication otAuth;

        public long EnterpriseVolumeID
        {
            get
            {
                return enterpriseVolumeID;
            }
        }

        public long CategoryVolumeID
        {
            get
            {
                return categoryVolumeID;
            }
        }

        public OTAuthentication OtAuth
        {
            get
            {
                return otAuth;
            }

            set
            {
                otAuth = value;
            }
        }

        public DocManClient(OTAuthentication otAuth, BasicHttpBinding basicHttpBinding, EndpointAddress endpointAddress) : base(basicHttpBinding, endpointAddress)
        {
            this.otAuth = otAuth;
            try
            {
                enterpriseVolumeID = GetEnterpriseVolumeNodeID();
                categoryVolumeID = GetCategoryVolumeNodeID();
            }
            catch (FaultException e)
            {
                throw new CWSException(string.Format("{0} : {1}\n", e.Code.Name, e.Message), e);
            }
        }

        public Support_CWS_16_22.Node GetNode(long nodeID)
        {
            Support_CWS_16_22.Node cwsNode = null;
            try
            {
                cwsNode = GetNode(ref otAuth, nodeID);
            }
            catch (FaultException e)
            {
                throw new CWSException(string.Format("{0} : {1}\n", e.Code.Name, e.Message), e);
            }

            return cwsNode;
        }

        /// <summary>
        /// Gets a CWS folder node
        /// </summary>
        /// <param name="folderID">folder ID</param>
        /// <returns>Folder object</returns>
        public Folder GetFolderNode(long folderID)
        {
            Support_CWS_16_22.Node cwsNode = null;
            try
            {
                cwsNode = GetNode(ref otAuth, folderID);
            }
            catch (FaultException e)
            {
                throw new CWSException(string.Format("{0} : {1}\n", e.Code.Name, e.Message), e);
            }

            if (cwsNode == null || cwsNode.VolumeID != EnterpriseVolumeID)
            {
                throw new LivelinkNodeDoesNotExistException(string.Format("Folder(ID:'{0}') doesn't exists in the Enterprise Volume", folderID));
            }

            if (!cwsNode.Type.Equals("Folder"))
            {
                throw new LivelinkInvalidNodeException(string.Format("Node(ID:'{0}' VolumeID:'{1}') is not a Folder", folderID, EnterpriseVolumeID));
            }

            Callback callback = NodeCallBack;

            return AdapterUtility.From(cwsNode, callback);
        }

        /// <summary>
        /// Creates a structure of CWS folder nodes
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="collection"></param>
        /// <param name="mode">overwrite if exists or throw error</param>
        /// <returns>FoldersCollection object</returns>
        public FoldersCollection CreateFolderStructure(long parentID, FoldersCollection collection, NodeExistsCreateMode mode)
        {
            FoldersCollection folderStructure = new FoldersCollection();

            foreach (Folder folder in collection)
            {
                Folder newFolder = CreateFolderNode(parentID, folder, mode);

                if (folder.Folders.Count > 0)
                {
                    newFolder.Folders = CreateFolderStructure(newFolder.ID, folder.Folders, mode);
                }

                folderStructure.Add(newFolder);
            }

            return folderStructure;
        }

        /// <summary>
        /// Creates a CWS folder node
        /// </summary> 
        /// <param name="parentID">parent ID</param>
        /// <param name="folder">folder object containg all update information and categories to apply to the folder</param>
        /// <param name="mode">overwrite if exists or throw error</param>
        /// <returns>Folder object</returns>
        public Folder CreateFolderNode(long parentID, Folder folder, NodeExistsCreateMode mode)
        {
            Metadata folderMetadata;

            Support_CWS_16_22.Node node = null;
            List<long> folderCatIds = new List<long>();
            List<AttributeGroup> attGroups = new List<AttributeGroup>();
            List<AttributeGroup> templates = new List<AttributeGroup>();
            Metadata metadata = new Metadata();
            Callback callback = NodeCallBack;

            try
            {
                // check parent is in enterprise volume
                if (GetNodeVolumeID(parentID) != enterpriseVolumeID)
                {
                    throw new LivelinkNodeDoesNotExistException(string.Format("Parent(ID:'{0}') doesn't exist in Enterprise Volume", parentID));
                }

                // check node already exists
                node = GetNodeByName(ref otAuth, parentID, folder.Name);
                node = (node != null && node.Type == "Folder") ? node : null;

                if (node != null)
                {
                    switch (mode)
                    {
                        case NodeExistsCreateMode.ReturnError:
                            {
                                throw new LivelinkNodeExistsException(string.Format("Folder(ID:'{0}' VolumeID:'{1}') already exists", node.ID, EnterpriseVolumeID));
                            }

                        case NodeExistsCreateMode.Read:
                            {
                                return AdapterUtility.From(node, callback);
                            }

                        default:
                            break;
                    }
                }

                if (folder.Categories.Count > 0)
                {
                    templates = GetTemplatesforCollection(folder.Categories);
                    folderMetadata = AdapterUtility.From(folder.Categories, templates);
                    folderCatIds = folderMetadata.AttributeGroups.Select(s => GetCatIdFromKey(s.Key)).ToList();
                    metadata.AttributeGroups = folderMetadata.AttributeGroups;
                }

                CategoryInheritance[] categoryInheritances = GetCategoryInheritance(ref otAuth, parentID);

                if (categoryInheritances != null)
                {
                    foreach (CategoryInheritance categoryInheritance in categoryInheritances)
                    {
                        if (categoryInheritance.Inheritance == true)
                        {
                            if (GetNodeVolumeID(categoryInheritance.CategoryID) == categoryVolumeID)
                            {
                                if (!folderCatIds.Contains(categoryInheritance.CategoryID))
                                {
                                    AttributeGroup attGroup = GetNode(ref otAuth, parentID).Metadata.AttributeGroups.FirstOrDefault(s => GetCatIdFromKey(s.Key).Equals(categoryInheritance.CategoryID));
                                    attGroups.Add(attGroup);
                                }
                            }
                            else
                            {
                                throw new LivelinkCategoryDoesNotExistException(string.Format("Parent Category(ID:'{0}') doesn't exist in the Category Volume", categoryInheritance.CategoryID));
                            }
                        }
                    }
                }

                AttributeGroup[] parentGroups = attGroups.ToArray();
                metadata.AttributeGroups = (metadata.AttributeGroups != null) ? metadata.AttributeGroups.Union(parentGroups).ToArray() : parentGroups;

                if (node != null && mode == NodeExistsCreateMode.Overwrite)
                {
                    SetNodeMetadata(ref otAuth, node.ID, metadata);
                    node = GetNode(ref otAuth, node.ID);
                }
                else
                {
                    node = CreateFolder(ref otAuth, parentID, folder.Name, null, metadata); //comment=null, unused
                }
            }
            catch (FaultException e)
            {
                throw new CWSException(string.Format("{0} : {1}\n", e.Code.Name, e.Message), e);
            }
            catch (AdapterException e)
            {
                throw new CWSException(e.Message, e);
            }

            return AdapterUtility.From(node, callback);
        }

        /// <summary>
        /// Returns all categories from the root of the Content Server Category Volume
        /// </summary>
        /// <returns>CategoriesCollection object</returns>
        public CategoriesCollection GetInstanceCategories()
        {
            CategoriesCollection collection = new CategoriesCollection();
            Support_CWS_16_22.Node[] cats;

            GetNodesInContainerOptions options = new GetNodesInContainerOptions();
            options.MaxDepth = 0;
            options.MaxResults = int.MaxValue;

            cats = GetNodesInContainer(ref otAuth, Math.Abs(CategoryVolumeID), options);

            if (cats != null)
            {
                for (int i = 0; i < cats.Length; i++)
                {
                    if (cats[i].Type == "Category")
                    {
                        collection.Add(AdapterUtility.FromCat(cats[i]));
                    }
                }
            }

            return collection;
        }

        private long GetCategoryVolumeNodeID()
        {
            return GetRootNode(ref otAuth, "CategoriesWS").VolumeID;
        }

        private long GetEnterpriseVolumeNodeID()
        {
            return GetRootNode(ref otAuth, "EnterpriseWS").VolumeID;
        }

        private long? GetNodeVolumeID(long nodeID)
        {
            Support_CWS_16_22.Node node = GetNode(ref otAuth, nodeID);

            return (node == null) ? (long?)null : node.VolumeID;
        }

        private List<AttributeGroup> GetTemplatesforCollection(CategoriesCollection collection)
        {
            List<AttributeGroup> templates = new List<AttributeGroup>();
            AttributeGroup template = new AttributeGroup();

            foreach (Category category in collection)
            {
                if (category.ID > 0)
                {
                    template = GetCategoryTemplate(ref otAuth, category.ID);
                }
                else
                {
                    Category catWOID = GetInstanceCategories()[category.Name];
                    if (catWOID == null)
                    {
                        throw new LivelinkCategoryDoesNotExistException(string.Format("Category(Name:'{0}') doesn't exist in the Category Volume", category.Name));
                    }

                    template = GetCategoryTemplate(ref otAuth, catWOID.ID);
                }

                templates.Add(template);
            }

            return templates;
        }

        public delegate Support_CWS_16_22.Node Callback(long id);

        private Support_CWS_16_22.Node NodeCallBack(long id)
        {
            return GetNode(ref otAuth, id);
        }
    }
}
