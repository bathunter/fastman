﻿namespace MWC.Livelink.Interface.WebService.TestHarness
{
    partial class Encrypt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textStringToEncrypt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textEncryptedString = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonEcrypt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textStringToEncrypt
            // 
            this.textStringToEncrypt.Location = new System.Drawing.Point(84, 7);
            this.textStringToEncrypt.Name = "textStringToEncrypt";
            this.textStringToEncrypt.Size = new System.Drawing.Size(213, 20);
            this.textStringToEncrypt.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "String to encrypt";
            // 
            // textEncryptedString
            // 
            this.textEncryptedString.Location = new System.Drawing.Point(84, 33);
            this.textEncryptedString.Name = "textEncryptedString";
            this.textEncryptedString.Size = new System.Drawing.Size(213, 20);
            this.textEncryptedString.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Encrypted string";
            // 
            // buttonClear
            // 
            this.buttonClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClear.Location = new System.Drawing.Point(166, 59);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 18;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonEcrypt
            // 
            this.buttonEcrypt.Location = new System.Drawing.Point(85, 59);
            this.buttonEcrypt.Name = "buttonEcrypt";
            this.buttonEcrypt.Size = new System.Drawing.Size(75, 23);
            this.buttonEcrypt.TabIndex = 17;
            this.buttonEcrypt.Text = "Encrypt";
            this.buttonEcrypt.UseVisualStyleBackColor = true;
            this.buttonEcrypt.Click += new System.EventHandler(this.buttonEcrypt_Click);
            // 
            // Encrypt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonEcrypt);
            this.Controls.Add(this.textEncryptedString);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textStringToEncrypt);
            this.Controls.Add(this.label4);
            this.Name = "Encrypt";
            this.Size = new System.Drawing.Size(300, 151);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox textStringToEncrypt;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox textEncryptedString;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Button buttonClear;
        protected System.Windows.Forms.Button buttonEcrypt;

    }
}
