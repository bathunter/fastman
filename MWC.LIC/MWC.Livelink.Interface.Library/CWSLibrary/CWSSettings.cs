﻿namespace MWC.Livelink.Interface.CWSLibrary
{
    public class CWSSettings
    {
        public const string UrlAuthSvc = "Authentication.svc";

        public const string UrlDocManSvc = "DocumentManagement.svc";

        public const string UrlContentSvc = "ContentService.svc";

        private string cwsBaseUrl;

        private int cwsPort;

        private bool ssoEnabled;

        private string userName;

        private string password;

        private string impersonateUser;

        public string CwsBaseUrl
        {
            get
            {
                return cwsBaseUrl;
            }

            set
            {
                cwsBaseUrl = value;
            }
        }

        public int CwsPort
        {
            get
            {
                return cwsPort;
            }

            set
            {
                cwsPort = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        public string ImpersonateUser
        {
            get
            {
                return impersonateUser;
            }

            set
            {
                impersonateUser = value;
            }
        }

        public bool SSOEnabled
        {
            get
            {
                return ssoEnabled;
            }

            set
            {
                ssoEnabled = value;
            }
        }

        public CWSSettings()
        {
        }

        public CWSSettings(string cwsBaseUrl, int cwsPort, string userName, string password)
        {
            this.cwsBaseUrl = cwsBaseUrl.EndsWith("/") ? cwsBaseUrl : string.Concat(cwsBaseUrl, "/");
            this.cwsPort = cwsPort;
            this.userName = userName;
            this.password = password;
        }
    }
}
