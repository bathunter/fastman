﻿using System;
using System.Collections.Generic;
using System.Web;

namespace MWC.Livelink.Interface.WebService
{
    /// <summary>
    /// Class used to pass Livelink connection information to the web service methods
    /// </summary>
    [Serializable]
    public class LivelinkSession
    {
        public enum AuthenticationType
        {
            SingleSignOn,
            Livelink
        }
        public AuthenticationType Authentication;
        public string LivelinkUser;
        public string Password;
        public string ImpersonateUser;

        private LivelinkSession()
        { }
        public LivelinkSession(AuthenticationType authentication, string livelinkUser, string password, string impersonateUser)
        {
            if (authentication == AuthenticationType.Livelink)
            {
                if (livelinkUser.Trim() == string.Empty || password.Trim() == string.Empty)
                {
                    throw new LivelinkConnectionException("You must provide Livelink user name and password for Livelink authentication.");
                }
            }
            Authentication = authentication;
            LivelinkUser = livelinkUser;
            Password = password;
            ImpersonateUser = impersonateUser;
        }
    }
}
