using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Collections;
using MWC.Livelink.Interface.CWSLibrary;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Action enum for Node creation if Node already exists.
    /// -Overwire - will update the node
    /// -Read - will read the node information and return
    /// -ReturnError - will return error if node already exist
    /// </summary>
    public enum NodeExistsCreateMode
    {
        Overwrite,
        Read,
        ReturnError
    }

    /// <summary>
    /// Main class that exposes public functions. Wrapper for CWSClient.
    /// </summary>
    public class LivelinkManager : IDisposable
    {
        private CWSClient cwsClient;

        /// <summary>
        /// Initialise LivelinkManager with Livelink instance.
        /// </summary>
        /// <param name="livelinkInstance">LivelinkInstance object populated with Livelink connection information</param>
        public LivelinkManager(LivelinkInstance livelinkInstance)
        {
            //create CWSClient with the configured settings
            CWSSettings cwsSetting = new CWSSettings();
            string cwsBaseUrl = string.Format("{0}{1}",
                livelinkInstance.LivelinkServer,
                livelinkInstance.LivelinkCGI.StartsWith("/") ? livelinkInstance.LivelinkCGI : string.Concat("/", livelinkInstance.LivelinkCGI));
            cwsSetting.CwsBaseUrl = cwsBaseUrl.EndsWith("/") ? cwsBaseUrl : String.Concat(cwsBaseUrl, "/");
            cwsSetting.CwsPort = livelinkInstance.LivelinkPortNo;
            cwsSetting.SSOEnabled = livelinkInstance.LivelinkSingleSignOnEnabled;
            if (!cwsSetting.SSOEnabled)
            {
                cwsSetting.UserName = livelinkInstance.LivelinkUserName;
                cwsSetting.Password = livelinkInstance.LivelinkUserPassword;
                try
                {
                    cwsSetting.UserName = StringEncryptor.DecryptData(cwsSetting.UserName);
                    cwsSetting.Password = StringEncryptor.DecryptData(cwsSetting.Password);
                }
                catch
                {
                    // username/password are not ecncrypted
                }
            }
            cwsSetting.ImpersonateUser = livelinkInstance.ImpersonateLivelinkUser;
            cwsClient = new CWSClient(cwsSetting);
        }

        /// <summary>
        /// Returns all categories defined in the Livelink instance Category Volume
        /// </summary>
        /// <returns></returns>
        public CategoriesCollection GetInstanceCategories()
        {
            return cwsClient.GetInstanceCategories();
        }

        /// <summary>
        /// Get a folder by its object id
        /// </summary>
        /// <param name="folderID">folder ID</param>
        /// <returns>Folder object</returns>
        public Folder GetFolder(int folderID)
        {
            return cwsClient.GetFolder(folderID);
        }
        /// <summary>
        /// Create folder object
        /// </summary>
        /// <param name="parentID">parent ID</param>
        /// <param name="folderInfo">folder object containg all update information and categories to apply to the folder</param>
        /// <param name="uploadMode">overwrite if exists or throw error</param>
        /// <returns></returns>
        public Folder CreateFolder(int parentID, Folder folderInfo, NodeExistsCreateMode uploadMode)
        {
            return cwsClient.CreateFolder(parentID, folderInfo, uploadMode);
        }

        /// <summary>
        /// Create folder objects
        /// </summary>
        /// <param name="parentID">parent ID</param>
        /// <param name="foldersInfo">folders collection object containg all folders update information and categories to apply to the folders</param>
        /// <param name="uploadMode">overwrite if exists or throw error</param>
        /// <returns></returns>
        public FoldersCollection CreateFolderStructure(int parentID, FoldersCollection foldersInfo, NodeExistsCreateMode uploadMode)
        {
            return cwsClient.CreateFolderStructure(parentID, foldersInfo, uploadMode);
        }

        public void Dispose()
        {
            cwsClient.Dispose();
        }
    }
}