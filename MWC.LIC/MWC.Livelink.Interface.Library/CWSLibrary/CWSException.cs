﻿namespace MWC.Livelink.Interface.CWSLibrary
{
    using System;

    class CWSException : Exception
    {
        private string codeName;

        public string CodeName
        {
            get
            {
                return codeName;
            }

            set
            {
                codeName = value;
            }
        }

        public CWSException(string message) : base(message)
        {
        }

        public CWSException(string message, string codeName) : base(message)
        {
            CodeName = codeName;
        }

        public CWSException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public CWSException(string message, string codeName, Exception innerException) : base(message, innerException)
        {
            CodeName = codeName;
        }
    }
}
