﻿using MWC.Livelink.Interface;
using MWC.Livelink.Interface.CWSLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace TestCWS
{
    class Program
    {
        static void Main(string[] args)
        {
            CWSSettings settings = new CWSSettings("http://10.10.108.18/les-services", 80, "admin", "livelink");
            //settings.SSOEnabled = true;
            CWSClient client = new CWSClient(settings);


            //NodeExistsCreateMode mode = NodeExistsCreateMode.Overwrite;

            Folder folder = new Folder("TestCreationDate");

            MWC.Livelink.Interface.Attribute attString = new MWC.Livelink.Interface.Attribute(2, "StringField");
            attString.Value = "there";

            MWC.Livelink.Interface.Attribute attInt = new MWC.Livelink.Interface.Attribute(3, "IntField");
            attInt.Value = 1010;

            MWC.Livelink.Interface.Attribute attBool = new MWC.Livelink.Interface.Attribute(4, "FlagField");
            attBool.Value = false;

            MWC.Livelink.Interface.Attribute attDate = new MWC.Livelink.Interface.Attribute(5, "DateField");
            attDate.Value = new DateTime(1995, 3, 31);

            AttributesCollection attColl = new AttributesCollection();
            attColl.Add(attString);
            attColl.Add(attInt);
            attColl.Add(attDate);
            attColl.Add(attBool);

            Category cat = new Category(225733, "SimpleString");
            cat.Attributes = attColl;

            CategoriesCollection collection = new CategoriesCollection();
            collection.Add(cat);

            folder.Categories = collection;

            CategoriesCollection cn =  client.GetInstanceCategories();

            StringBuilder output = new StringBuilder();
            var writer = new System.IO.StringWriter(output);

            XmlSerializer serializer = new XmlSerializer(cn.GetType());
            serializer.Serialize(writer, cn);

            Console.WriteLine(output.ToString());

            writer.Close();


            //client.GetFolder(234416);


            


        }
    }
}
