﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MWC.Livelink.Interface.WebService.TestHarness
{
    public partial class BaseControl : UserControl
    {
        public delegate void RunClickedHandler();
        public delegate void CancelClickedHandler();
        public event RunClickedHandler RunClicked;
        public event CancelClickedHandler CancelClicked;

        protected virtual void OnRunClicked()
        {
            if (RunClicked != null)
            {
                RunClicked();
            }
        }

        protected virtual void OnCancelClicked()
        {
            if (CancelClicked != null)
            {
                CancelClicked();
            }
        }

        public BaseControl()
        {
            InitializeComponent();
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            OnRunClicked();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            OnCancelClicked();
        }
    }
}
