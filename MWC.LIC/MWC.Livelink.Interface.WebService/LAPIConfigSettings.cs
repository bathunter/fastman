﻿using System;
using System.Collections;
using System.Configuration;


namespace MWC.Livelink.Interface.WebService
{
    /// <summary>
    /// Class representing LAPI configuration settings. It reads the configuration defined in the web.config
    /// </summary>
    public class LAPIConfigSettings : ConfigurationSection
    {
        private static ConfigurationProperty _livelinkserver;
        private static ConfigurationProperty _livelinkcgi;
        private static ConfigurationProperty _livelinkport;
        private static ConfigurationProperty _ssoport;
        private static ConfigurationPropertyCollection _properties;

        static LAPIConfigSettings()
        {
            _livelinkserver = new ConfigurationProperty("LivelinkServer", typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _livelinkcgi = new ConfigurationProperty("LivelinkCGI", typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _livelinkport = new ConfigurationProperty("LivelinkPort", typeof(int), null, ConfigurationPropertyOptions.IsRequired);
            _ssoport = new ConfigurationProperty("SSOPort", typeof(int), null, ConfigurationPropertyOptions.IsRequired);

            _properties = new ConfigurationPropertyCollection();
            _properties.Add(_livelinkserver);
            _properties.Add(_livelinkcgi);
            _properties.Add(_livelinkport);
            _properties.Add(_ssoport);
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get { return _properties; }
        }
        [ConfigurationProperty("LivelinkServer")]
        public string LivelinkServer
        {
            get
            {
                return (string)base[_livelinkserver];
            }
            set
            {
                base[_livelinkserver] = value;
            }
        }
        [ConfigurationProperty("LivelinkCGI")]
        public string LivelinkCGI
        {
            get
            {
                return (string)base[_livelinkcgi];
            }
            set
            {
                base[_livelinkcgi] = value;
            }
        }
        [ConfigurationProperty("LivelinkPort")]
        public int LivelinkPort
        {
            get
            {
                return (int)base[_livelinkport];
            }
            set
            {
                base[_livelinkport] = value;
            }
        }
        [ConfigurationProperty("SSOPort")]
        public int SSOPort
        {
            get
            {
                return (int)base[_ssoport];
            }
            set
            {
                base[_ssoport] = value;
            }
        }
    }

   
    /// <summary>
    ///  Use by web service to return LAPIConfigSettings
    /// </summary>
    public class InstanceSettings
    {
        public string LivelinkServer;
        public string LivelinkCGI;
        public int LivelinkPort;
        public int SSOPort;
    }

}
