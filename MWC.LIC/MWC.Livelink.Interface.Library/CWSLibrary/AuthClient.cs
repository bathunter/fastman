﻿namespace MWC.Livelink.Interface.CWSLibrary
{
    using Support_CWS_16_22;
    using System.ServiceModel;

    class AuthClient : AuthenticationClient
    {
        private readonly string userName;

        private readonly string password;

        private readonly string impersonateUser;

        private OTAuthentication otAuth;

        public OTAuthentication OtAuth
        {
            get
            {
                return otAuth;
            }
        }

        public AuthClient(BasicHttpBinding basicHttpBinding, EndpointAddress endpointAddress, string userName, string password, string impersonateUser) : base(basicHttpBinding, endpointAddress)
        {
            this.userName = userName;
            this.password = password;
            this.impersonateUser = impersonateUser;
            try
            {
                otAuth = new OTAuthentication();
                SetOtAuthToken();
            }
            catch (FaultException e)
            {
                throw new CWSException(string.Format("{0} : {1}\n", e.Code.Name, e.Message), e.Code.Name, e);
            }
        }

        /// <summary>
        /// Performs authentication in CS
        /// </summary>
        /// <returns>Authentication token</returns>
        /// <exception cref="System.ServiceModel.FaultException">Thrown in case of Authentication failure in CS</exception>
        public void SetOtAuthToken()
        {
            try
            {
                otAuth.AuthenticationToken = AuthenticateUser(userName, password);
                if (!string.IsNullOrEmpty(impersonateUser))
                {
                    string impersonateUserToken = ImpersonateUser(ref otAuth, impersonateUser);
                    otAuth.AuthenticationToken = impersonateUserToken;
                }
            }
            catch (FaultException e)
            {
                throw new CWSException(string.Format("{0} : {1}\n", e.Code.Name, e.Message), e.Code.Name, e);
            }
        }

        public bool UpdateToken()
        {
            bool tokenUpdated = false;
            try
            {
                GetSessionExpirationDate(ref otAuth);
            }
            catch (FaultException)
            {
                {
                    SetOtAuthToken();
                    tokenUpdated = true;
                }
            }

            return tokenUpdated;
        }
    }
}
