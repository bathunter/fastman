﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Class representing Livelink Document object.
    /// </summary>
    [Serializable]
    public class Document : Node
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Document()
            : base()
        {

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ID">Document ID</param>
        public Document(int ID)
            : base(ID)
        {

        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Name">Document Name</param>
        public Document(string Name)
            : base(Name)
        {

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ID">Document ID</param>
        /// <param name="Name">Document Name</param>
        public Document(int ID, string Name)
            : base(ID, Name)
        {

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="VolumeID">Document Volume ID</param>
        /// <param name="ID">Document ID</param>
        public Document(int VolumeID, int ID)
            : base(VolumeID, ID)
        {

        }
        #endregion
    }
}
