using System;

namespace MWC.Livelink.Interface
{
    /// <summary>
    /// Class representing Livelink category.
    /// </summary>
    [Serializable]
    public class Category : Node
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Category() : base()
        {
            _attributes = new AttributesCollection();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ID">Category ID</param>
        public Category(int ID) : base(ID)
        {
            _attributes = new AttributesCollection();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Name">Category Name</param>
        public Category(string Name) : base(Name)
        {
            _attributes = new AttributesCollection();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ID">Category ID</param>
        /// <param name="Name">Category Name</param>
        public Category(int ID, string Name): base(ID, Name)
        {
            _attributes = new AttributesCollection();
        }
        #endregion
        AttributesCollection _attributes;
        bool _inheritance = true;

        /// <summary>
        /// Flag indicating if the category will be inherited by
        /// child nodes when applied on a container node
        /// </summary>
        public bool Inhertiance
        {
            get
            {
                return _inheritance;
            }
            set
            {
                _inheritance = value;
            }
        }

        /// <summary>
        /// Category attributes
        /// </summary>
        public AttributesCollection Attributes
        {
            get
            {
                return _attributes;
            }
            set
            {
                _attributes = value;
            }
        }
    }
}
