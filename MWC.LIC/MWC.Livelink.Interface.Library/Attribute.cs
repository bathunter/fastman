using System;
using System.Collections;
using System.Collections.Generic;

namespace MWC.Livelink.Interface
{
   
    /// <summary>
    /// Class representing Livelink category attribute.
    /// </summary>
    [Serializable]
    public class Attribute
    {
        #region Fields
        private int _id;
        private string _name;
        private object _value;
        private object _defaultvalue;
        private string[] _validvalues;
        private bool _mandatory;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Attribute()
        {

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ID">Attribute ID</param>
        /// <param name="Name">Attribute Name</param>
        public Attribute(int ID, string Name)
        {
            _id = ID;
            _name = Name;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Name">Attribute Name</param>
        /// <param name="Value">Attribute Value</param>
        public Attribute(string Name, object Value)
        {
            _name = Name;
            _value = Value;
        }

        /*internal Attribute(LLValue attribute)
        {
            LoadAttribute(attribute);
        }*/
        #endregion
    
        #region Properties
        /// <summary>
        /// Attribute ID
        /// </summary>
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        /// <summary>
        /// Attribute Name
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// Attribute Value
        /// </summary>
        public object Value
        {
            get
            {
                if(_value!=null && _value is string) ((string)_value).Replace(Environment.NewLine, " ");
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// Attribute default value
        /// </summary>
        public object DefaultValue
        {
            get
            {
                return _defaultvalue;
            }
            set
            {
                _defaultvalue = value;
            }
        }

        /// <summary>
        /// Flag whether the attribute is mandatory or not
        /// </summary>
        public bool Mandatory
        {
            get
            {
                return _mandatory;
            }
            set
            {
                _mandatory = value;
            }
        }

        /// <summary>
        /// List of valid values if the attribute is select type
        /// </summary>
        public string[] ValidValues
        {
            get
            {
                return _validvalues;
            }
            set
            {
                _validvalues = value;
            }
        }
        #endregion

    }
}
