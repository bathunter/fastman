﻿namespace MWC.Livelink.Interface.CWSLibrary
{
    using System;
    using System.ServiceModel;

    /// <summary>
    /// CWSClient class that manipulates Content Server objects trough CWS
    /// </summary>
    public class CWSClient : IDisposable
    {
        private const int MaxReceivedMessageSize = int.MaxValue;

        private AuthClient authClient;

        private DocManClient docManClient;

        private bool isDisposed = false;

        /// <summary>
        /// Initialise CWSClient with Content Server settings.
        /// </summary>
        /// <param name="settings">settings are populated from LivelinkInstance object</param>
        public CWSClient(CWSSettings settings)
        {
            try
            {
                ////instantiation of AuthClient
                authClient = new AuthClient(
                                            GetDefaultHttpBinding(settings.SSOEnabled),
                                            GetDefaultEndPointAddress(settings.CwsBaseUrl, settings.CwsPort, CWSSettings.UrlAuthSvc),
                                            settings.UserName,
                                            settings.Password,
                                            settings.ImpersonateUser);
                ////instantiation of DocManClient
                BasicHttpBinding docBinding = GetDefaultHttpBinding(settings.SSOEnabled);
                docBinding.MaxReceivedMessageSize = MaxReceivedMessageSize;
                docManClient = new DocManClient(
                                                authClient.OtAuth,
                                                docBinding,
                                                GetDefaultEndPointAddress(settings.CwsBaseUrl, settings.CwsPort, CWSSettings.UrlDocManSvc));
            }
            catch (CWSException e)
            {
                if (e.CodeName.Equals("Core.LoginFailed"))
                {
                    throw new LivelinkSessionCreationException(e.Message);
                }

                throw new UnhandledLivelinkException(e.Message);
            }
            catch (CommunicationException e)
            {
                throw new LivelinkConnectionException(e.Message);
            }
        }

        /// <summary>
        /// Gets a CWS folder node
        /// </summary>
        /// <param name="folderID">folder ID</param>
        /// <returns>Folder object</returns>
        public Folder GetFolder(long folderID)
        {
            try
            {
                UpdateToken();
                return docManClient.GetFolderNode(folderID);
            }
            catch (CWSException e)
            {
                throw new UnhandledLivelinkException(e.Message);
            }

        }

        /// <summary>
        /// Creates a CWS folder node
        /// </summary> 
        /// <param name="parentID">parent ID</param>
        /// <param name="folder">folder object containg all update information and categories to apply to the folder</param>
        /// <param name="mode">overwrite if exists or throw error<</param>
        /// <returns>Folder object</returns>
        public Folder CreateFolder(long parentID, Folder folder, NodeExistsCreateMode mode)
        {
            if (folder == null || folder.Name == null || folder.Name.Trim() == string.Empty)
            {
                throw new LivelinkInvalidNodeException(string.Format("Supplied folder object is not valid. Folder name is blank."));
            }

            try
            {
                UpdateToken();
                return docManClient.CreateFolderNode(parentID, folder, mode);
            }
            catch (CWSException e)
            {

                throw new UnhandledLivelinkException(e.Message);
            }
        }

        /// <summary>
        /// Creates a structure of CWS folder nodes
        /// </summary>
        /// <param name="parentID">parent ID</param>
        /// <param name="collection">folder structure object containg all update information and categories to apply to the folders</param>
        /// <param name="mode">overwrite if exists or throw error</param>
        /// <returns>FoldersCollection object</returns>
        public FoldersCollection CreateFolderStructure(long parentID, FoldersCollection collection, NodeExistsCreateMode mode)
        {
            try
            {
                UpdateToken();
                return docManClient.CreateFolderStructure(parentID, collection, mode);
            }
            catch (CWSException e)
            {
                throw new UnhandledLivelinkException(e.Message);
            }
        }

        /// <summary>
        /// Returns all categories the root of the Content Server Category Volume
        /// </summary>
        /// <returns>CategoriesCollection object</returns>
        public CategoriesCollection GetInstanceCategories()
        {
            try
            {
                UpdateToken();
                return docManClient.GetInstanceCategories();
            }
            catch (CWSException e)
            {

                throw new UnhandledLivelinkException(e.Message);
            }
        }

        private void UpdateToken()
        {
            if (authClient.UpdateToken())
            {
                docManClient.OtAuth.AuthenticationToken = authClient.OtAuth.AuthenticationToken;
            }
        }

        private EndpointAddress GetDefaultEndPointAddress(string cwsBaseUrl, int port, string svcUrl)
        {
            UriBuilder uriBuilder;
            try
            {
                uriBuilder = new UriBuilder(string.Concat(cwsBaseUrl, svcUrl));
                uriBuilder.Port = port;
            }
            catch (UriFormatException)
            {
                throw new LivelinkConnectionException("Please check Livelink connection information");
            }

            return new EndpointAddress(uriBuilder.Uri);
        }

        private BasicHttpBinding GetDefaultHttpBinding(bool ssoEnabled)
        {
            BasicHttpBinding httpBinding = new BasicHttpBinding();
            if (ssoEnabled)
            {
                httpBinding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
                httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            }

            return httpBinding;
        }

        public static long GetCatIdFromKey(string categoryKey)
        {
            return Convert.ToInt64(categoryKey.IndexOf(".") == -1 ? categoryKey : categoryKey.Substring(0, categoryKey.IndexOf(".")));
        }

        public static int GetAttrIdFromKey(string attributeKey)
        {
            return Convert.ToInt32(attributeKey.LastIndexOf(".") == -1 ? attributeKey : attributeKey.Substring(attributeKey.LastIndexOf(".") + 1));
        }

        /// <summary>
        /// The Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    if (docManClient != null)
                    {
                        docManClient.Close();
                    }

                    if (authClient != null)
                    {
                        authClient.Close();
                    }
                }

                isDisposed = true;
            }
        }
    }
}
